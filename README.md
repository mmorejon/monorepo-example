# Monorepo Example

Repository to test GitLab CI/CD feature.

```mermaid
sequenceDiagram
    participant root
    participant projects
    participant tai as terraform-aws-instance
    participant tas as terraform-aws-sg
    root->>projects: include
    par trigger downstreams pipelines
        projects->>tai: trigger
        tai->>tai: include terraform template
    and
        projects->>tas: trigger
        tas->>tas: include terraform template
    end
```
